<?php
// $Id: node.tpl.php,v 1.3 2010/07/02 23:14:21 eternalistic Exp $
?>
<?php
  function get_node_link($dir = 'next', $n, $next_node_text = NULL, $prepend_text= NULL, $append_text = NULL) {
    $query = "SELECT n.nid, n.title FROM {node} n WHERE n.created " .  
      ($dir == 'previous' ? '<' : '>') . '"%d" AND n.type = "%s" AND n.status=1 ORDER BY n.created ' . ($dir == 'previous' ? 'DESC' : 'ASC');
    $result = db_query($query, $n->created, $n -> type);
    if ($row = db_fetch_object($result)) {
      $text = $next_node_text ? $next_node_text : $row -> title;
      return $prepend_text . l($text, 'node/' . $row->nid, array('rel' => $dir)) . $append_text;
    } 
    return NULL;
  }
?>
<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?>">
  <div class="inner">
    <?php print $picture ?>

    <?php if ($page == 0): ?>
    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php endif; ?>

    <?php if ($submitted): ?>
    <div class="meta">
      <span class="submitted"><?php print $submitted ?></span>
    </div>
    <?php endif; ?>

    <?php if ($node_top && !$teaser): ?>
    <div id="node-top" class="node-top row nested">
      <div id="node-top-inner" class="node-top-inner inner">
        <?php print $node_top; ?>
      </div><!-- /node-top-inner -->
    </div><!-- /node-top -->
    <?php endif; ?>

    <div class="content clearfix">
      <?php print $content ?>
    </div>

    <?php if ($terms): ?>
    <div class="terms">
      <div class="terms-inner">
        <?php print $terms; ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($links): ?>
    <div class="links">
      <div class="links-inner">
        <?php print $links; ?>
      </div>
    </div>
    <?php endif; ?>
  </div><!-- /inner -->

  <?php if ($node_bottom && !$teaser): ?>
  <div id="node-bottom" class="node-bottom row nested">
    <div id="node-bottom-inner" class="node-bottom-inner inner">
      <?php print $node_bottom; ?>
    </div><!-- /node-bottom-inner -->
  </div><!-- /node-bottom -->
  <?php endif; ?>
</div><!-- /node-<?php print $node->nid; ?> -->
<?php
  if ($page != 0) {
    $previous_node_link = get_node_link('previous', $node, '< Previous Testimonial');
    $next_node_link = get_node_link('next', $node, 'Next Testimonial >');
    print '<div id="node-testimonial-nav">';
    if($previous_node_link && $next_node_link) {
      print $previous_node_link.' | '. $next_node_link;
    } else if ($previous_node_link) {
      print $previous_node_link;
    }
    else if ($next_node_link) {
      print $next_node_link;
    }
    print '</div>';
  }                    
?>
